---
title: Kola Heyward - Rotimi
alias: Kola
projects:
- https://www.kolaheywardrotimi.com/
posts:
- 4df3cc40-0b0c-4a38-835d-fa4a69f991bf
order: 15
draft: false
layout: author
uuid: 32a7b17c-8a97-45ca-8939-2d6822d2bf66
liquid: false
usuaries:
- 356
- 534
last_modified_at: 2023-03-15 17:10:50.770276279 +00:00
---

<p><strong>Kola</strong> is a writer and new media artist/scholar from Durham, NC. He is currently a Faculty Research Assistant at the University of Maryland’s African American History, Culture, and Digital Humanities initiative (AADHum), where he studies how different societies engage with and create virtual spaces.</p>