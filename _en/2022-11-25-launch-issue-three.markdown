---
title: LAUNCH ISSUE THREE!
description: We're still looking for funding for future issues! Help us reach our
  goal!
value_max: 25000.0
currency: "$"
order: 16
draft: false
layout: goal
uuid: 2568b8c9-bf51-404c-8af5-621b8ee488bd
liquid: false
usuaries:
- 356
- 534
last_modified_at: 2023-03-15 17:10:50.869846876 +00:00
---

