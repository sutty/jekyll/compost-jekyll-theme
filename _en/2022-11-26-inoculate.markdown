---
title: INOCULATE
subtitle: ISSUE 02
image:
  path: public/fmtp5z6wy9r80xg8qrtklvso4uis/compost_02_cover.gif
  description: ''
order: 22
draft: false
layout: front_cover
uuid: 3ca5c2cd-a849-449b-b759-a847025ae7c1
liquid: false
usuaries:
- 356
- 616
last_modified_at: 2023-03-15 17:10:50.989542188 +00:00
---

<p>As dominant platforms continue to construct an unimaginative reality of sleekness, convenience, and extraction, we wonder: How do we <em>Inoculate</em> networks with our consideration and attention, against the flattening, homogenizing forces of the internet?</p><p>This second issue of COMPOST magazine takes a step back; widening our scope and probing how we shape digital networks and how they shape us back.</p>
