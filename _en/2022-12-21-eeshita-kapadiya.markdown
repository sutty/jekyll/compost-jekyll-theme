---
title: Eeshita Kapadiya
alias: Eeshita
projects:
- https://killjoycooking.network/
posts:
- 2f8cf6d6-2e93-4f0a-9acb-953647a17e0e
order: 27
draft: false
layout: author
uuid: a741354e-b1cf-42cd-9419-c3d3322b9f1d
liquid: false
usuaries:
- 27
- 534
last_modified_at: 2023-03-15 17:10:51.322053341 +00:00
---

<p><strong>Eeshita</strong> is an artist and designer based in Bangalore. She mostly likes to cook, sew and dream up ways to smash the patriarchy.</p>
