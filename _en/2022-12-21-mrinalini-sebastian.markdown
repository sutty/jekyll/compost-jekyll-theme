---
title: Mrinalini Sebastian
alias: Mrinalini Sebastian
projects: []
posts:
- 2f8cf6d6-2e93-4f0a-9acb-953647a17e0e
order: 29
draft: false
layout: author
uuid: 827c5715-0127-4271-b6cc-1bdb021253a3
liquid: false
usuaries:
- 1
- 27
- 534
last_modified_at: 2023-03-15 17:10:51.360340670 +00:00
---

<p><strong>Mrinalini Sebastian</strong> from Puttur, Karnataka, is an independent researcher who is interested in questions of history, community, gender, and critical theories. She works part-time as the Site Coordinator of Harcum College Partnership Site at Grace-Trinity UCC, Philadelphia and mentors a small group of pre-school teachers who are working towards their Associate degree.</p>
