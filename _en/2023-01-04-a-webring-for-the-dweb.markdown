---
title: A WEBRING FOR THE DWEB!
description: We'll launch a webring for the Decentralized Web to make allied web and
  DWeb projects more discoverable.
value_max: 30000.0
currency: "$"
order: 13
draft: false
layout: goal
uuid: a1e96bde-f3f1-421d-a27d-0f09b2a168ac
liquid: false
usuaries:
- 534
last_modified_at: 2023-03-15 17:10:51.535281616 +00:00
---

