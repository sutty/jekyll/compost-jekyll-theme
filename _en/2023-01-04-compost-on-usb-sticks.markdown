---
title: COMPOST ON USB sticks!
description: Take COMPOST offline and start a COMPOST sneakernet with our USB sticks
  filled with COMPOST art and writing.
value_max: 35000.0
currency: "$"
order: 9
draft: false
layout: goal
uuid: 60c2b1ac-27d1-44ab-80f6-7051acfa12d2
liquid: false
usuaries:
- 534
last_modified_at: 2023-03-15 17:10:51.569070328 +00:00
---

