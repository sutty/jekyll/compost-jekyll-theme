---
title: HOST A MICROCONFERENCE!
description: We'll run a half-day micro-conference with contributors and community.
  We'll discuss issues we face on the (D)Web and in collective publishing.
value_max: 40000.0
currency: "$"
order: 4
draft: false
layout: goal
uuid: 3f6470eb-c9d3-48fc-8139-251848c22233
liquid: false
usuaries:
- 534
last_modified_at: 2023-03-15 17:10:51.610715945 +00:00
---

